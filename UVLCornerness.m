function CValue = UVLCornerness ( I, Sigma )

% Test the input parameters
error ( nargchk ( 2, 2, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

% Test input type Image => Convert if it is not Real
if ~isa ( I, 'double' )
    I = double ( I );
end

% Prewitt/Sobel filter for the derivatives
dx = [ -1 0 1; -2 0 2; -1 0 1 ];

% Perform the convolution
Ix = conv2 ( I, dx, 'same' );
Iy = conv2 ( I, dx', 'same' );

if Sigma ~= 0
    % Create a Gaussian filter using the input Sigma
    % The Size is proportial to this input Sigma
    mf = max ( 1, fix ( 6*Sigma ) );
    Gaussian = fspecial ( 'gaussian', mf, Sigma );

    % Compute Ix^2, Iy^2, Ix*Iy and filter
    Ix2 = conv2 ( Ix .* Ix, Gaussian, 'same' );
    Iy2 = conv2 ( Iy .* Iy, Gaussian, 'same' );
    Ixy = conv2 ( Ix .* Iy, Gaussian, 'same' );
else
    % Compute Ix^2, Iy^2, Ix*Iy and filter
    Ix2 = Ix .* Ix;
    Iy2 = Iy .* Iy;
    Ixy = Ix .* Iy;
end

% Add an epsilon to the divider to avoid by 0 divisions
CValue = ( Ix2 .* Iy2 - Ixy .* Ixy ) ./ ( Ix2 + Iy2 + eps );

% Normalization of the cornerness matrix
CValue = CValue ./ max(max(CValue));

% % Forming the Frames; take only with Cornerness >= Threshold
% FramesInd = find ( C >= Threshold );
% [ FramesY, FramesX ] = ind2sub ( size(C), FramesInd );
% FramesS = transpose(sortrows([FramesX,FramesY,C(FramesInd)],-3));
% 
% % Spreading the features --------------------------------------------------
% 
% [h,w] = size(I);
% Frames = SpreadFeatures ( FramesS, 3, h, w, Mask, Radius, ...
%                           MaxNumber, Threshold );
% 
% % Perform Sub-Pixel Accuracy if required ----------------------------------
% 
% Rows = Frames(2,:);
% Columns = Frames(1,:);
% 
% if DoSubPixel > 0 && ~isempty(Rows) && ~isempty(Columns)
%     [ROffs, COffs] = SubPixel ( C, Rows', Columns', 1 );
%     Frames(1,:) = Frames(1,:) + COffs';
%     Frames(2,:) = Frames(2,:) + ROffs';
% end