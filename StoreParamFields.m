function Param = StoreParamFields(Param, handles)

    % [General]
    Param.General.SaveLog = get(handles.CheckBoxSaveLog, 'Value');
    Param.General.NumCorresp = str2num(get(handles.EditNumCorresp, 'String'));
    Param.General.MinMatches = str2num(get(handles.EditMinMatches, 'String'));

    % [Image preprocessing]
    Param.Processing.SingleChannel = get(handles.PopUpMenuSingleChannel, 'Value') - 1;
    Param.Processing.NonUniformLightCorrection = get(handles.CheckBoxNonUniformLightCorrection, 'Value');
    Param.Processing.GaussianSize = str2num(get(handles.EditGaussianSize, 'String'));
    Param.Processing.Equalization = get(handles.PopUpMenuEqualization, 'Value') - 1;
    Param.Processing.ResizeCoeff = str2num(get(handles.EditResizeCoeff, 'String'));

    % [Detector]
    Param.Detection.Type = get(handles.PopUpMenuDetectorType, 'Value');
    
    switch Param.Detection.Type
        case 1
            Param.Detection.Harris.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.Harris.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.Harris.Border = str2num(get(handles.EditBorder, 'String'));
        case 2
            Param.Detection.Laplacian.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.Laplacian.RadiusNonMaximal = str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.Laplacian.Border = str2num(get(handles.EditBorder, 'String'));
        case 3
            Param.Detection.Hessian.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.Hessian.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.Hessian.Border = str2num(get(handles.EditBorder, 'String'));            
        case 4
            Param.Detection.SIFT.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.SIFT.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.SIFT.Border = str2num(get(handles.EditBorder, 'String'));
        case 5
            Param.Detection.SURF.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.SURF.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.SURF.Border = str2num(get(handles.EditBorder, 'String'));
        case 6
            Param.Detection.MSER.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.MSER.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.MSER.Border = str2num(get(handles.EditBorder, 'String'));
            Param.Detection.MSER.MinimumRegionSize = str2num(get(handles.EditMinimumRegionSize, 'String'));
            Param.Detection.MSER.MinimumMargin = str2num(get(handles.EditMinimumMargin, 'String'));
        case 7
            Param.Detection.HarLap.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.HarLap.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.HarLap.Border = str2num(get(handles.EditBorder, 'String'));
        case 8
            Param.Detection.HesLap.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.HesLap.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.HesLap.Border = str2num(get(handles.EditBorder, 'String'));
        case 9
            Param.Detection.HarAff.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.HarAff.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.HarAff.Border = str2num(get(handles.EditBorder, 'String'));
        case 10
            Param.Detection.HesAff.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.HesAff.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.HesAff.Border = str2num(get(handles.EditBorder, 'String'));
        case 11
            Param.Detection.HarHesLap.MaxNumber = str2num(get(handles.EditMaxNumber, 'String'));
            Param.Detection.HarHesLap.RadiusNonMaximal =str2num(get(handles.EditRadiusNonMaximal, 'String'));
            Param.Detection.HarHesLap.Border = str2num(get(handles.EditBorder, 'String'));
    end

    % [Descriptors]
    Param.Description.Type = get(handles.PopUpMenuDescriptorType, 'Value');
    Param.Description.RelativeOrientation = get(handles.PopUpMenuRelativeOrientation, 'Value') - 1;

    % [Matchers]
    Param.Matching.Type = get(handles.PopUpMenuMatchingType, 'Value');
    Param.Matching.ErrorBounds = str2num(get(handles.EditErrorBounds, 'String'));
    Param.Matching.Thresh = str2num(get(handles.EditThresh, 'String'));
    
    % [Outlier Rejection]
    Param.Homo.Type = get(handles.PopUpMenuHomoType, 'Value');

    switch get(handles.PopUpMenuHomoModel, 'Value');
        case 1
            Param.Homo.Model = 'euc';
        case 2
            Param.Homo.Model = 'sim';
        case 3
            Param.Homo.Model = 'aff';
        case 4
            Param.Homo.Model = 'pro';
        case 5
            Param.Homo.Model = 'nlp';
    end

    Param.Homo.StDev = str2double(get(handles.EditStDev, 'String'));
    Param.Homo.MaxTrials = str2num(get(handles.EditMaxTrials, 'String'));

    % [Plotting]
    Param.Plotting.DoShowCorrespAfterRejection = get(handles.CheckBoxDoShowCorrespAfterRejection, 'Value');
    Param.Plotting.DoShowPoints = get(handles.CheckBoxDoShowPoints, 'Value');
    Param.Plotting.DoShowCorrespBeforeRejection = get(handles.CheckBoxDoShowCorrespBeforeRejection, 'Value');
    Param.Plotting.DoPauseAfterPlot = get(handles.CheckBoxDoPauseAfterPlot, 'Value');
    
    % [Refine]
    Param.Refine.InitialGuess = get(handles.PopUpMenuInitialGuess, 'Value');
    
    switch get(handles.PopUpMenuRefineHomoModel, 'Value');
        case 1
            Param.Refine.HomoModel = 'euc';
        case 2
            Param.Refine.HomoModel = 'sim';
        case 3
            Param.Refine.HomoModel = 'aff';
        case 4
            Param.Refine.HomoModel = 'pro';
        case 5
            Param.Refine.HomoModel = 'nlp';
    end
    
    Param.RefineCorel.CorrelRadius = str2num(get(handles.EditCorrelRadius, 'String'));
    Param.RefineCorel.SearchRadius = str2num(get(handles.EditSearchRadius, 'String'));
    Param.RefineCorel.CorrelThresh = str2num(get(handles.EditCorrelThresh, 'String'));
    Param.RefineCorel.PyramidLevel = str2num(get(handles.EditPyramidLevel, 'String'));
    Param.RefineCorel.MaxCorresp = str2num(get(handles.EditMaxCorresp, 'String'));
    
    % !!!!! UVL_GMML_refine and UVL_GMML_refineCorrelation should be merged in
    % UVL_GMML_run using Param.Refine.InitialGuess = 0 for normal processing

end