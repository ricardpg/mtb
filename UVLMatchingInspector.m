function varargout = UVLMatchingInspector(varargin)
% UVLMATCHINGINSPECTOR MATLAB code for UVLMatchingInspector.fig
%      UVLMATCHINGINSPECTOR, by itself, creates a new UVLMATCHINGINSPECTOR or raises the existing
%      singleton*.
%
%      H = UVLMATCHINGINSPECTOR returns the handle to a new UVLMATCHINGINSPECTOR or the handle to
%      the existing singleton*.
%
%      UVLMATCHINGINSPECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UVLMATCHINGINSPECTOR.M with the given input arguments.
%
%      UVLMATCHINGINSPECTOR('Property','Value',...) creates a new UVLMATCHINGINSPECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before UVLMatchingInspector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to UVLMatchingInspector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help UVLMatchingInspector

% Last Modified by GUIDE v2.5 12-Dec-2011 09:59:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @UVLMatchingInspector_OpeningFcn, ...
                   'gui_OutputFcn',  @UVLMatchingInspector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before UVLMatchingInspector is made visible.
function UVLMatchingInspector_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to UVLMatchingInspector (see VARARGIN)

% Choose default command line output for UVLMatchingInspector
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

    global Mosaic;
    global MosaicFileName;
    global iPairPrev;
    global hPopUpImage1;
    global hPopUpImage2;

    hPopUpImage1 = 0;
    hPopUpImage2 = 0;

    if nargin == 4

        MosaicFileName = varargin{1};
        Mosaic = UVL_GMML_loadStructure(MosaicFileName);
        iPairPrev = 0;

        FillUITablePairs(hObject, eventdata, handles);

        if numel(Mosaic.Nodes) > 1
            eventdata.Indices(1) = 1;
            UITablePairs_CellSelectionCallback(hObject, eventdata, handles);
        end

    end

% keyboard

% UIWAIT makes UVLMatchingInspector wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = UVLMatchingInspector_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% % --- Executes on button press in LoadImagePair.
% function LoadImagePair_Callback(hObject, eventdata, handles)
% % hObject    handle to LoadImagePair (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
%     global Mosaic;
%     global List;
%     % global I1;
%     % global I2;
% 
%     i1_i = List(get(handles.ListBoxPairs, 'Value'), 1);
%     i2_i = List(get(handles.ListBoxPairs, 'Value'), 2);
% 
%     I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
%     I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
% 
%     set(handles.UIPanelImage1, 'Title', sprintf('Image %03d', i1_i));
%     set(handles.UIPanelImage2, 'Title', sprintf('Image %03d', i2_i));
% 
%     imshow(I1, 'Parent', handles.AxesImage1);
%     imshow(I2, 'Parent', handles.AxesImage2);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% % --- Executes on selection change in ListBoxPairs.
% function ListBoxPairs_Callback(hObject, eventdata, handles)
% % hObject    handle to ListBoxPairs (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hints: contents = cellstr(get(hObject,'String')) returns ListBoxPairs contents as cell array
% %        contents{get(hObject,'Value')} returns selected item from ListBoxPairs
% 
%     global Mosaic;
%     % global List;
%     global LocalList;
%     global Points;
% 
%     iPair = get(handles.ListBoxPairs, 'Value');
% 
%     if iPair > 0
%         i1_i = LocalList(iPair, 1);
%         i2_i = LocalList(iPair, 2);
% 
%         I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
%         I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
% 
%         set(handles.UIPanelImage1, 'Title', sprintf('Image %03d', i1_i));
%         set(handles.UIPanelImage2, 'Title', sprintf('Image %03d', i2_i));
% 
%         axes(handles.AxesImage1); hold off;
%         imshow(I1, 'Parent', handles.AxesImage1);
%         axes(handles.AxesImage2); hold off;
%         imshow(I2, 'Parent', handles.AxesImage2);
% 
%         if Points(iPair).nPoints > 0    
%             axes(handles.AxesImage1);
%             hold on;
%             plot(Points(iPair).I1Points(1, :), Points(iPair).I1Points(2, :), '+g');
% 
%             if get(handles.CheckBoxPointNumbersImage1, 'Value')
%                 for i = 1 : Points(iPair).nPoints
%                     text(Points(iPair).I1Points(1, i) + 5, Points(iPair).I1Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
%                 end
%             end
% 
%             axes(handles.AxesImage2);
%             hold on;
%             plot(Points(iPair).I2Points(1, :), Points(iPair).I2Points(2, :), '+g');
% 
%             if get(handles.CheckBoxPointNumbersImage2, 'Value')
%                 for i = 1 : Points(iPair).nPoints
%                     text(Points(iPair).I2Points(1, i) + 5, Points(iPair).I2Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
%                 end
%             end
%         end
% 
%     end


% --- Executes during object creation, after setting all properties.
function ListBoxPairs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListBoxPairs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on button press in PushButtonClickPoints.
function PushButtonClickPoints_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonClickPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% figure(handles.AxesImage1);
% axes(handles.AxesImage1);

    global Mosaic;
    % global List;
    global LocalList;
    global hI1;
    global hI2;
    global Points;
    global iPairPrev;
    % global MissingCorrespondences;

    if Points(iPairPrev).corrFromPairWise
        Answer = questdlg({'This image pair was already successfully matched.', 'Do you want to proceed anyway ?'}, 'UVLMatchingInspector');

        if ~strcmp(Answer, 'Yes')
            return
        end
    end
        
    % Image 1 Points
    i1_i = LocalList(iPairPrev, 1);
    hI1 = figure('Name', sprintf('Image %03d', i1_i));
    I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
    warning('off', 'images:initSize:adjustingMag');
    imshow(I1); hold on;
    warning('on', 'images:initSize:adjustingMag');
    iPair = iPairPrev;

    Points(iPair).nPoints = 0;
    Points(iPair).I1Points = [];
    [I1x I1y] = ginput(1);
    Points(iPair).I1Points = [I1x I1y]';
    plot(I1x, I1y, '+g');

    if get(handles.CheckBoxPointNumbersImage1, 'Value')
        text(I1x + 5, I1y - 5, sprintf('%d', Points(iPair).nPoints + 1), 'Color', 'yellow');
    end

    while numel([I1x I1y]) > 0
        Points(iPair).nPoints = Points(iPair).nPoints + 1;
        [I1x I1y] = ginput(1);
        plot(I1x, I1y, '+g');

        if get(handles.CheckBoxPointNumbersImage1, 'Value')
            text(I1x + 5, I1y - 5, sprintf('%d', Points(iPair).nPoints + 1), 'Color', 'yellow');
        end

        Points(iPair).I1Points = [Points(iPair).I1Points [I1x I1y]'];
    end

    axes(handles.AxesImage1); hold off;
    imshow(I1, 'Parent', handles.AxesImage1); hold on;
    plot(Points(iPair).I1Points(1, :), Points(iPair).I1Points(2, :), '+g');

    if get(handles.CheckBoxPointNumbersImage1, 'Value')
        for i = 1 : Points(iPair).nPoints
            text(Points(iPair).I1Points(1, i) + 5, Points(iPair).I1Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
        end
    end

    % if Points(iPair).nPoints > 0
    %     StaticText = get(handles.ListBoxPairs, 'String');
    %     StaticText{iPair} = sprintf('%03d --- %03d ---> Done with %03d correspondences', List(MissingCorrespondences(iPair), 1), List(MissingCorrespondences(iPair), 2), Points(iPair).nPoints);
    %     set(handles.ListBoxPairs, 'String', StaticText);
    % end

    % Image 2 Points
    i2_i = LocalList(iPairPrev, 2);
    hI2 = figure('Name', sprintf('Image %03d', i2_i));
    I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
    warning('off', 'images:initSize:adjustingMag');
    imshow(I2); hold on;
    warning('off', 'images:initSize:adjustingMag');
    % iPair = get(handles.ListBoxPairs, 'Value');

    Points(iPair).I2Points = [];

    for i = 1 : Points(iPair).nPoints
        [I2x I2y] = ginput(1);
        plot(I2x, I2y, '+g');

        if get(handles.CheckBoxPointNumbersImage2, 'Value')
            text(I2x + 5, I2y - 5, sprintf('%d', i), 'Color', 'yellow');
        end

        Points(iPair).I2Points = [Points(iPair).I2Points [I2x I2y]'];
    end

    axes(handles.AxesImage2); hold off;
    imshow(I2, 'Parent', handles.AxesImage2); hold on;
    plot(Points(iPair).I2Points(1, :), Points(iPair).I2Points(2, :), '+g');

    if get(handles.CheckBoxPointNumbersImage2, 'Value')
        for i = 1 : Points(iPair).nPoints
            text(Points(iPair).I2Points(1, i) + 5, Points(iPair).I2Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
        end
    end

    if Points(iPair).nPoints > 0
        UITablePairsData = get(handles.UITablePairs, 'Data');
        UITablePairsData(iPair, 2) = {Points(iPair).nPoints};
        set(handles.UITablePairs, 'Data', UITablePairsData);
    end
%     else
%         msgbox('This image pair was already successfully matched !', 'UVLMatchingInspector');
%     end


% --- Executes on button press in PustButtonStorePoints.
function PustButtonStorePoints_Callback(hObject, eventdata, handles)
% hObject    handle to PustButtonStorePoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global LocalList;
    global Points;
    global iPairPrev;
    
    if size(Points(iPairPrev).I1Points, 2) == size(Points(iPairPrev).I2Points, 2)
        Points(iPairPrev).corrFromPairWise = true;
        LocalList(iPairPrev, 3) = Points(iPairPrev).nPoints;
        fprintf('nPoints = %d\n', Points(iPairPrev).nPoints);

        UITablePairsData = get(handles.UITablePairs, 'Data');
        UITablePairsData(iPairPrev, 3) = {true};
        set(handles.UITablePairs, 'Data', UITablePairsData);

        FileName = sprintf('Corresp-%05d-%05d.bpm', LocalList(iPairPrev, 1), LocalList(iPairPrev, 2));
        UVL_GMML_saveCorrespondenceFile([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' FileName], [Points(iPairPrev).I1Points; Points(iPairPrev).I2Points]);

        if numel(Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges) > 0
            pos = find([Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(:).NodeIndex] == LocalList(iPairPrev, 2));

            if numel(pos) > 0
                Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(pos).Corresp = FileName;
            else
                Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end + 1).NodeIndex = LocalList(iPairPrev, 2);
                Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).MergeIndex = 0;
                Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).Corresp = FileName;
                Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).Homo.Matrix = zeros(3);
                Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).Homo.Model = 'nlp';
            end
        else
            Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end + 1).NodeIndex = LocalList(iPairPrev, 2);
            Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).MergeIndex = 0;
            Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).Corresp = FileName;
            Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).Homo.Matrix = zeros(3);
            Mosaic.Nodes(LocalList(iPairPrev, 1)).Edges(end).Homo.Model = 'nlp';
        end
    else
        msgbox('The number of correspondences does not match !', 'UVLMatchingInspector');
    end


% --- Executes on mouse press over axes background.
function AxesImage1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to AxesImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in PushButtonClickPointsImage2.
function PushButtonClickPointsImage2_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonClickPointsImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in PushButtonSaveGMML.
function PushButtonSaveGMML_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonSaveGMML (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global MosaicFileName;

    AnswerSave = questdlg({'The current GMML file will be overwritten.', 'Do you want to proceed anyway ?'}, 'UVLMatchingInspector');

    if strcmp(AnswerSave, 'Yes')

        AnswerCompute = questdlg('Do you want to compute the absolute homographies ?', 'UVLMatchingInspector');
        
        if strcmp(AnswerCompute, 'Yes')
            Mosaic = UVL_GMML_computeAbsoluteFromCorrespondences(Mosaic, 'nlp', 1, numel(Mosaic.Nodes));
        end
    
        UVL_GMML_saveStructure(Mosaic, MosaicFileName);
    end


% --- Executes on button press in CheckBoxFeaturesImage1.
function CheckBoxFeaturesImage1_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxFeaturesImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxFeaturesImage1

    global Mosaic;
    global LocalList;
    global Points;
    global iPairPrev;

    if Points(iPairPrev).nPoints > 0

        axes(handles.AxesImage1); hold off;
        i1_i = LocalList(iPairPrev, 1);
        I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
        imshow(I1, 'Parent', handles.AxesImage1); hold on;

        if get(handles.CheckBoxFeaturesImage1, 'Value')

            set(handles.CheckBoxPointNumbersImage1, 'Enable', 'on');
            plot(Points(iPairPrev).I1Points(1, :), Points(iPairPrev).I1Points(2, :), '+g');

            if get(handles.CheckBoxPointNumbersImage1, 'Value')
                for i = 1 : Points(iPairPrev).nPoints
                    text(Points(iPairPrev).I1Points(1, i) + 5, Points(iPairPrev).I1Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
                end
            end
            
        else

            set(handles.CheckBoxPointNumbersImage1, 'Enable', 'off');
%             i1_i = LocalList(iPairPrev, 1);
%             axes(handles.AxesImage1); hold off;
%             I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image], 'JPG');
%             imshow(I1, 'Parent', handles.AxesImage1);
            
        end

    end


% --- Executes on button press in CheckBoxFeaturesImage2.
function CheckBoxFeaturesImage2_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxFeaturesImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxFeaturesImage2

    global Mosaic;
    global LocalList;
    global Points;
    global iPairPrev;

    if Points(iPairPrev).nPoints > 0

        axes(handles.AxesImage2); hold off;
        i2_i = LocalList(iPairPrev, 2);
        I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
        imshow(I2, 'Parent', handles.AxesImage2); hold on;

        if get(handles.CheckBoxFeaturesImage2, 'Value')

            set(handles.CheckBoxPointNumbersImage2, 'Enable', 'on');
            plot(Points(iPairPrev).I2Points(1, :), Points(iPairPrev).I2Points(2, :), '+g');

            if get(handles.CheckBoxPointNumbersImage2, 'Value')
                for i = 1 : Points(iPairPrev).nPoints
                    text(Points(iPairPrev).I2Points(1, i) + 5, Points(iPairPrev).I2Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
                end
            end
            
        else

            set(handles.CheckBoxPointNumbersImage2, 'Enable', 'off');
%             i2_i = LocalList(iPairPrev, 2);
%             axes(handles.AxesImage2); hold off;
%             I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image], 'JPG');
%             imshow(I2, 'Parent', handles.AxesImage2);
            
        end

    end


function FillUITablePairs(hObject, eventdata, handles)

    global Mosaic;
    global LocalList;
    global Points;

%     UITablePairsData = get(handles.UITablePairs, 'Data');
    UITablePairsData = cell(1, 3);
    n = 0;
    LocalList = [];
    Points = [];

    % Build Pairs List
    for i = 1 : numel(Mosaic.Nodes)
        if numel(Mosaic.Nodes(i).Edges) > 0
            for j = 1 : numel(Mosaic.Nodes(i).Edges)
                n = n + 1;
                corr = UVL_GMML_loadCorrespondenceFile([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' Mosaic.Nodes(i).Edges(j).Corresp]);
                n_corr = size(corr, 2);
                UITablePairsData(n, 1) = {sprintf('%03d - %03d', i, Mosaic.Nodes(i).Edges(j).NodeIndex)};
                UITablePairsData(n, 2) = {n_corr};
                UITablePairsData(n, 3) = {logical(n_corr)};
                LocalList(n, :) = [i Mosaic.Nodes(i).Edges(j).NodeIndex n_corr];
                Points(n).nPoints = n_corr;
                Points(n).I1Points = corr(1 : 2, :);
                Points(n).I2Points = corr(3 : 4, :);
                Points(n).corrFromPairWise = (n_corr > 0);
            end
        else
            if i > 1
                n = n + 1;
                UITablePairsData(n, 1) = {sprintf('%03d - %03d', i, i - 1)};
                UITablePairsData(n, 2) = {0};
                UITablePairsData(n, 3) = {false};
                LocalList(n, :) = [i (i - 1) 0];
                Points(n).nPoints = 0;
                Points(n).I1Points = [];
                Points(n).I2Points = [];
                Points(n).corrFromPairWise = false;
            end
        end
    end

%     UITablePairsData(1 : end - 1, :) = UITablePairsData(2 : end, :);
    set(handles.UITablePairs, 'Data', UITablePairsData);

%     for i = 1 : numel(Points)
% 
%         corr = UVL_GMML_loadCorrespondenceFile([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' Mosaic.Nodes(i).Edges(i).Corresp]);
%         n_corr = size(corr, 2);
% 
%         UITablePairs(i, 1) = sprintf('%03d --- %03d with %03d corr.', i, Mosaic.Nodes(i).Edges.NodeIndex, n_corr)
%         if Points(i).corrFromPairWise
% 
% 
%             set(handles.UITablePairsdata, 'Data', UITablePairsData); 
%         end
%     end


% function CreateEdge(iEdge)
% 
%     global LocalList;
%     global Points;
%     global i1_i;
%     global i2_i;
% 
%     LocalList = [LocalList(1 : iEdge - 1, :); i1_i i2_i 0; LocalList(iEdge : end, :)];
% 
%     nPoints = numel(Points);
% 
%     if nPoints > 1
%         for i = 1 : iEdge - 1
%             PointsAux(i) = Points(i);
%         end
% 
%         PointsAux(iEdge).nPoints = 0;
%         PointsAux(iEdge).I1Points = [];
%         PointsAux(iEdge).I2Points = [];
%         PointsAux(iEdge).corrFromPairWise = 0;
% 
%         for i = iEdge + 1 : nPoints + 1
%             PointsAux(i) = Points(i);
%         end
% 
%         Points = PointsAux;
%     end
% 
% 
% function DeleteEdge(iEdge)
% 
%     global LocalList;
%     global Points;
% 
%     LocalList(iEdge) = [];
%     
%     nPoints = numel(Points);
%     
%     if nPoints > 1
%         for i = 1 : iEdge - 1
%             PointsAux(i) = Points(i);
%         end
% 
%         for i = iEdge : nPoints - 1
%             PointsAux(i) = Points(i + 1);
%         end
% 
%         Points = PointsAux;
%     end


% --- Executes when selected cell(s) is changed in UITablePairs.
function UITablePairs_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to UITablePairs (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% data = get(tablehandle,'Data')
% data(event.indices(1),event.indices(2)) = pi(); 
% set(tablehandle,'Data',data); 

    global Mosaic;
    global LocalList;
    global Points;
    global iPairPrev;
    global i1_i;
    global i2_i;

%     UITablePairsData = get(handles.UITablePairs, 'Data');

    if numel(eventdata.Indices) > 0

        iPair = eventdata.Indices(1);

        if iPair ~= iPairPrev

            set(handles.TextStatusI1, 'String', 'Loading ...'); drawnow;
            set(handles.TextStatusI2, 'String', 'Loading ...'); drawnow;

            iPairPrev = iPair;
            i1_i = LocalList(iPair, 1);
            i2_i = LocalList(iPair, 2);
            
            I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
            I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);

            set(handles.UIPanelImage1, 'Title', sprintf('Image %03d', i1_i));
            set(handles.UIPanelImage2, 'Title', sprintf('Image %03d', i2_i));

            axes(handles.AxesImage1); hold off;
            imshow(I1, 'Parent', handles.AxesImage1);
            axes(handles.AxesImage2); hold off;
            imshow(I2, 'Parent', handles.AxesImage2);

            if Points(iPair).nPoints > 0    
                if get(handles.CheckBoxFeaturesImage1, 'Value')
                    axes(handles.AxesImage1); hold on;
                    plot(Points(iPair).I1Points(1, :), Points(iPair).I1Points(2, :), '+g');

                    if get(handles.CheckBoxPointNumbersImage1, 'Value')
                        for i = 1 : Points(iPair).nPoints
                            text(Points(iPair).I1Points(1, i) + 5, Points(iPair).I1Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
                        end
                    end
                end

                if get(handles.CheckBoxFeaturesImage2, 'Value')
                    axes(handles.AxesImage2); hold on;
                    plot(Points(iPair).I2Points(1, :), Points(iPair).I2Points(2, :), '+g');

                    if get(handles.CheckBoxPointNumbersImage2, 'Value')
                        for i = 1 : Points(iPair).nPoints
                            text(Points(iPair).I2Points(1, i) + 5, Points(iPair).I2Points(2, i) - 5, sprintf('%d', i), 'Color', 'yellow');
                        end
                    end
                end
            end

            hPopUpImage1 = 0;
            hPopUpImage2 = 0;

        end

        if size(eventdata.Indices, 2) > 1
            if eventdata.Indices(2) == 2
                if LocalList(iPair, 3) > 0
                    UVL_GMML_showNodeCorrespondences(Mosaic, LocalList(iPair, 1), LocalList(iPair, 2));
                end
            end
        end

        set(handles.TextStatusI1, 'String', ''); drawnow;
        set(handles.TextStatusI2, 'String', ''); drawnow;

    end

% --- Executes on button press in CheckBoxPointNumbersImage1.
function CheckBoxPointNumbersImage1_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxPointNumbersImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxPointNumbersImage1

    axes(handles.AxesImage1); hold off;
    CheckBoxFeaturesImage1_Callback(hObject, eventdata, handles);

% --- Executes on button press in CheckBoxPointNumbersImage2.
function CheckBoxPointNumbersImage2_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxPointNumbersImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxPointNumbersImage2

    axes(handles.AxesImage2); hold off;
    CheckBoxFeaturesImage2_Callback(hObject, eventdata, handles);


% --- Executes on button press in PushButtonPopUpImage1.
function PushButtonPopUpImage1_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonPopUpImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global LocalList;
    global iPairPrev;
    global hPopUpImage1;

    i1_i = LocalList(iPairPrev, 1);
    I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
    
    if hPopUpImage1 ~= 0
        figure(hPopUpImage1); imshow(I1);
    else
        hPopUpImage1 = figure;
        imshow(I1);
    end

    set(hPopUpImage1, 'Name', sprintf('Image %03d', i1_i));


    % --- Executes on button press in PushButtonPopUpImage2.
function PushButtonPopUpImage2_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonPopUpImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global LocalList;
    global iPairPrev;
    global hPopUpImage2;

    i2_i = LocalList(iPairPrev, 2);
    I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
    
    if hPopUpImage2 ~= 0
        figure(hPopUpImage2); imshow(I2);
    else
        hPopUpImage2 = figure;
        imshow(I2);
    end

    set(hPopUpImage2, 'Name', sprintf('Image %03d', i2_i));


% --- Executes on button press in PushButtonRefinePoints.
function PushButtonRefinePoints_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonRefinePoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global LocalList;
    global Points;
    global iPairPrev;
    
    i1_i = LocalList(iPairPrev, 1);
    i2_i = LocalList(iPairPrev, 2);

    %% Load default parameters
    UVL_GMML_defaultParameters

    %% Try load specific parameters for sequence
    if exist('UVL_sequenceParameters.m', 'file')
        UVL_sequenceParameters
        fprintf('Using UVL_sequenceParameters.m\n');
    else
        warning('MATLAB:UVL_GMML_run', 'No sequence specific parameters found. Using default parameters');
    end

    %% Match image pair
    
    tic
    
    if ~exist('Cache', 'dir'), mkdir('Cache'); end
    UVL_SFM_printOut(sprintf('--- Node %d with Node %d ---\n', i2_i, i1_i));

    UVL_SFM_printOut(sprintf('\tExtracting features  >>'));
    ICur = []; FeaturesCur = []; % [ICur FeaturesCur] = UVL_SFM_loadCache(M.Nodes(no).ImageIndex, Param, CachePath{DsNo});
    if isempty(ICur),
        ICur = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
        ICur = imresize(ICur, 0.25);
        ICur = UVL_SFM_preprocessImage(ICur, i2_i, Param.Processing);
    end
    FeaturesCur = UVL_SFM_detectFeatures(ICur, FeaturesCur, Param.Detection);
%     if any(Param.Description.Type == [2 4 6]) && Param.Description.RelativeOrientation, % extend to sift (1) ?
%         if Param.Description.RelativeOrientation == 1,
%             Angle = -UVL_GMML_extractAngleFromH(M.Nodes(no).Homo.Matrix, [size(ICur,2)/2 size(ICur,1)/2]);
%         elseif Param.Description.RelativeOrientation == 2,
%             if numel(M.Nodes(no).Edges) >= edI;
%                 Angle = 0;
%             else
%                 UVL_SFM_printOut(sprintf(' - Edge does not exist - \n'));continue
%             end
%         else
%             error ('MATLAB:UVL_GMML_run', 'Relative orientation should be 1 (Absolute Homography) or 2 (Correspondences)');
%         end
%         AngleCode = ['O' num2str(round(Angle*100), '%03d') '.mat'];
%         FeaturesCur.Points(4,:) = round(Angle*100) / 100;
%         FeaturesCur.Descriptors = [];
%         FeaturesCur.DescriptionCacheFile = [FeaturesCur.DescriptionCacheFile(1:end-4) AngleCode];
%     end
    FeaturesCur = UVL_SFM_calculateDescriptors(ICur, FeaturesCur, Param.Description);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc));
    PrevTime = toc;

    UVL_SFM_printOut(sprintf('\tExtracting features  >>'));
    IRef = []; FeaturesRef = []; % [IRef FeaturesRef] = UVL_SFM_loadCache(M.Nodes(ed).ImageIndex, Param, CachePath{DsEd});
    if isempty(IRef),
        IRef = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
        IRef = imresize(IRef, 0.25);
        IRef = UVL_SFM_preprocessImage(IRef, i1_i, Param.Processing);
    end
    FeaturesRef = UVL_SFM_detectFeatures(IRef, FeaturesRef, Param.Detection);
%     if any(Param.Description.Type == [2 4 6]) && Param.Description.RelativeOrientation,
%         if Param.Description.RelativeOrientation == 1,
%             Angle = -UVL_GMML_extractAngleFromH(M.Nodes(ed).Homo.Matrix, [size(IRef,2)/2 size(IRef,1)/2]);
%             AngleCode = ['O' num2str(round(Angle*100), '%03d') '.mat'];
%             FeaturesRef.DescriptionCacheFile = [FeaturesRef.DescriptionCacheFile(1:end-4) AngleCode];
%             FeaturesRef.Points(4,:) = round(Angle*100) / 100;
%         elseif Param.Description.RelativeOrientation == 2,
%             if numel(M.Nodes(no).Edges) >= edI;
%                 InitH = UVL_SFM_getEuclideanTransformation(UVL_GMML_loadCorrespondenceFile(M.Nodes(no).Edges(edI).Corresp));
%                 Angle = UVL_GMML_extractAngleFromH(InitH, [size(IRef,2)/2 size(IRef,1)/2]);
%                 FeaturesRef.DescriptionCacheFile = '';
%                 FeaturesRef.Points(4,:) = Angle;
%             else
%                 UVL_SFM_printOut(sprintf(' - Edge does not exist - \n'));continue
%             end       
%         else
%             error ('MATLAB:UVL_GMML_run', 'Relative orientation should be 1 (Absolute Homography) or 2 (Correspondences)');
%         end
%         FeaturesRef.Descriptors = [];
%     end
    FeaturesRef = UVL_SFM_calculateDescriptors(IRef, FeaturesRef, Param.Description);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc;

    UVL_SFM_printOut(sprintf('\tMatches              >>'));
    Correspondences = UVL_GMML_match(FeaturesCur, FeaturesRef, Param.Matching);
    UVL_SFM_printOut(sprintf(' Total: %d Corr [%.1f sec]\n', size(Correspondences,2), toc-PrevTime));
    PrevTime = toc;

    if Param.Processing.ResizeCoeff ~= 1 && ~isempty(FeaturesRef.Points) && ~isempty(FeaturesCur.Points),
        FeaturesRef.Points(1:2,:) = FeaturesRef.Points(1:2,:) ./ Param.Processing.ResizeCoeff;
        FeaturesCur.Points(1:2,:) = FeaturesCur.Points(1:2,:) ./ Param.Processing.ResizeCoeff;
        Correspondences = Correspondences ./ Param.Processing.ResizeCoeff;
    end

    if  size(Correspondences, 2) > Param.General.MinMatches % && size(Correspondences, 2) > List(li,3) CAL MIRAR PERQU� SERVEIX !!!
        UVL_SFM_printOut(sprintf('\tReject outliers      >>'));
        if Param.Homo.StDev == 0,
            error ('MATLAB:UVL_GMML_run', 'LMedS not implemented yet. You should specify a Standard deviation');
        else
            [H, Inliers, Res] = UVL_SFM_ransacHomography(Correspondences, Param.Homo.Model, Param.Homo.StDev, Param.Homo.MaxTrials);
        end
        OptCorr = Correspondences(:,Inliers);
        NumCorr = sum(Inliers);
        UVL_SFM_printOut(sprintf(' %d remaining corr with residuals = %.2f [%.1f sec]\n', NumCorr, Res, toc-PrevTime));
        PrevTime = toc;
    else
        OptCorr = [];
        NumCorr = 0;
    end

    UVL_SFM_printOut(sprintf('\tPlotting             >>'));
    UVL_GMML_plotProcess(Param.Plotting, ICur, IRef, FeaturesCur, FeaturesRef, OptCorr, Correspondences);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));

%    FileName = sprintf('Corresp-%05d-%05d.bpm', LocalList(iPairPrev, 1), LocalList(iPairPrev, 2));
%    UVL_GMML_saveCorrespondenceFile([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' FileName], [Points(iPairPrev).I1Points; Points(iPairPrev).I2Points]);

    Points(iPairPrev).nPoints = NumCorr;
    Points(iPairPrev).I1Points = OptCorr(1 : 2, :);
    Points(iPairPrev).I2Points = OptCorr(3 : 4, :);

%     FillUITablePairs(hObject, eventdata, handles);

%     % FALTA FER APAR�IXER A ALGUN LLOC UN MISSATGE DURANT EL PROCESSAMENT
% 
%     I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
%     I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
% 
%     if size(I1, 3) > 1
%         I1gs = rgb2gray(I1);
%         I2gs = rgb2gray(I2);
%     else
%         I1gs = I1;
%         I2gs = I2;
%     end
% 
% %     % Refine Points in Image 1
% %     CImage = UVLCornerness(I1gs, 0.5);
% %     Radius = 5;
% % 
% %     for i = 1 : Points(iPairPrev).nPoints
% %         x = Points(iPairPrev).I1Points(1, i);
% %         y = Points(iPairPrev).I1Points(2, i);
% % 
% %         CPatch = CImage(y - Radius : y + Radius, x - Radius : x + Radius);
% %         [~, Position] = max(CPatch(:));
% %         [x_c y_c] = ind2sub([(Radius * 2 + 1) (Radius * 2 + 1)], Position);
% %         x_corner = x - Radius + x_c - 1;
% %         y_corner = y - Radius + y_c - 1;
% % 
% %         Points(iPairPrev).I1Points(:, i) = [x_corner y_corner]';
% %         [x y x_corner y_corner]
% % 
% %     end
% 
%     % Compute Euclidean transformation
%     H = UVL_SFM_getEuclideanTransformation([Points(iPairPrev).I1Points; Points(iPairPrev).I2Points]);
% %     [I2gst trans] = UVL_GMML_imTransform(I2gs, H);
% %     H
% %     trans
% 
%     Corr = UVL_GMML_extractCorrelCorresp(I1gs, I2gs, [Points(iPairPrev).I1Points; Points(iPairPrev).I2Points], 5, 0.5, 12, 1, Points(iPairPrev).nPoints, H);
%     Corr
%     keyboard
%     Points(iPairPrev).I2Points = Corr(3 : 4, :);
% 
% %     Corr
% % 	% Refine Matches in Image 2
% %     for i = 1 : Points(iPairPrev).nPoints
% %         
% %     end
% 
% %     figure; imshow(mat2gray(I2gst)); hold on;
% 
% %     for i = 1 : Points(iPairPrev).nPoints
% %         p2 = H * ([Points(iPairPrev).I2Points(:, i); 1] - [trans(1) trans(2) 0]');
% % %         p2 = H * [Points(iPairPrev).I2Points(:, i); 1];
% %         plot(p2(1), p2(2), 'xr');
% %     end

function ClickPoints2Images(hObject, eventdata, handles)

    


% --- Executes on button press in PushButtonCreateEdge.
function PushButtonCreateEdge_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonCreateEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global i1_i;
    global i2_i;

    if ismember(i2_i, [Mosaic.Nodes(i1_i).Edges.NodeIndex])
        errordlg(sprintf('The edge between nodes %d and %d already exists !', i1_i, i2_i), 'CreateEdgeError');
    else
        fprintf('Creating edge between nodes %d and %d ...\n', i1_i, i2_i);
        n_e = numel(Mosaic.Nodes(i1_i).Edges);

        Mosaic.Nodes(i1_i).Edges(n_e + 1).NodeIndex = i2_i;
        Mosaic.Nodes(i1_i).Edges(n_e + 1).MergeIndex = 0;
        Mosaic.Nodes(i1_i).Edges(n_e + 1).Corresp = sprintf('Corresp-%05d-%05d.bpm', i1_i, i2_i);
        Mosaic.Nodes(i1_i).Edges(n_e + 1).Homo.Model = 'nlp';
        Mosaic.Nodes(i1_i).Edges(n_e + 1).Homo.Matrix = zeros(3);

        if ~exist([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' Mosaic.Nodes(i1_i).Edges(n_e + 1).Corresp], 'file')
            UVL_GMML_saveCorrespondenceFile([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' Mosaic.Nodes(i1_i).Edges(n_e + 1).Corresp], []);
        end

%         i_e = 1;
% 
%         while i_e < i2_i
%             i_e = i_e + 1;
%         end
% 
%         for i = n_e : -1 : i_e
%             Mosaic.Nodes(i1_i).Edges(i + 1) = Mosaic.Nodes(i1_i).Edges(i);
%         end
% 
%         Mosaic.Nodes(i1_i).Edges(i_e).NodeIndex = i2_i;
%         Mosaic.Nodes(i1_i).Edges(i_e).MergeIndex = 0;
%         Mosaic.Nodes(i1_i).Edges(i_e).Corresp = sprintf('Corresp-%05d-%05d.bpm', i1_i, i2_i);
%         Mosaic.Nodes(i1_i).Edges(i_e).Homo.Model = 'pro';
%         Mosaic.Nodes(i1_i).Edges(i_e).Homo.Matrix = zeros(3);
%         end
% 
%         UVL_GMML_saveCorrespondenceFile([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' Mosaic.Nodes(i1_i).Edges(i_e).Corresp], []);

        FillUITablePairs(hObject, eventdata, handles);
    end


% --- Executes on button press in PushButtonPreviousImageImage2.
function PushButtonPreviousImageImage2_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonPreviousImageImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global i2_i;

    if i2_i > 1
        i2_i = i2_i - 1;

        set(handles.TextStatusI2, 'String', 'Loading ...'); drawnow;

        I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
        set(handles.UIPanelImage2, 'Title', sprintf('Image %03d', i2_i));
        imshow(I2, 'Parent', handles.AxesImage2);

        set(handles.TextStatusI2, 'String', ''); drawnow;
    end


% --- Executes on button press in PushButtonNextImageImage2.
function PushButtonNextImageImage2_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonNextImageImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global i2_i;
    global i1_i;

    if i2_i + 1 < i1_i
        i2_i = i2_i + 1;

        set(handles.TextStatusI2, 'String', 'Loading ...'); drawnow;

        I2 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i2_i).Image]);
        set(handles.UIPanelImage2, 'Title', sprintf('Image %03d', i2_i));
        imshow(I2, 'Parent', handles.AxesImage2);

        set(handles.TextStatusI2, 'String', ''); drawnow;        
    end


% --- Executes on button press in PushButtonNextImageImage1.
function PushButtonNextImageImage1_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonNextImageImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global i1_i;

    if i1_i <  numel(Mosaic.Nodes);
        i1_i = i1_i + 1;

        set(handles.TextStatusI1, 'String', 'Loading ...'); drawnow;
        
        I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
        set(handles.UIPanelImage1, 'Title', sprintf('Image %03d', i1_i));
        imshow(I1, 'Parent', handles.AxesImage1);
        
        set(handles.TextStatusI1, 'String', ''); drawnow;
    end

% --- Executes on button press in PushButtonPreviousImageImage1.
function PushButtonPreviousImageImage1_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonPreviousImageImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global i1_i;
    global i2_i;

    if i1_i > i2_i + 1
        i1_i = i1_i - 1;

        set(handles.TextStatusI1, 'String', 'Loading ...'); drawnow;

        I1 = imread([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).ImagePath '/' Mosaic.Nodes(i1_i).Image]);
        set(handles.UIPanelImage1, 'Title', sprintf('Image %03d', i1_i));
        imshow(I1, 'Parent', handles.AxesImage1);

        set(handles.TextStatusI1, 'String', ''); drawnow;
    end


% --- Executes on button press in PushButtonDeleteEdge.
function PushButtonDeleteEdge_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonDeleteEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Mosaic;
    global i1_i;
    global i2_i;

    if numel([Mosaic.Nodes(i1_i).Edges]) == 1
        errordlg(sprintf('A node with a single edge cannot be deleted !'), 'DeleteEdgeError');
    else
        fprintf('Deleting edge between nodes %d and %d ...\n', i1_i, i2_i);
        
        n_e = numel(Mosaic.Nodes(i1_i).Edges);
        
        i_e = 1;
        for i = 1 : n_e
            if Mosaic.Nodes(i1_i).Edges(i).NodeIndex ~= i2_i
                EdgesTemp(i_e) = Mosaic.Nodes(i1_i).Edges(i);
                i_e = i_e + 1;
            else
                CorrespFile = Mosaic.Nodes(i1_i).Edges(i).Corresp;
            end
        end
                    
        Mosaic.Nodes(i1_i).Edges = EdgesTemp;
%         delete([Mosaic.Header.Datasets(1).BasePath '/' Mosaic.Header.Datasets(1).CorrespPath '/' CorrespFile]);

        Answer = questdlg({'Do you want to delete the correspondences file', sprintf('%s ?', CorrespFile)}, 'UVLMatchingInspector');

        if strcmp(Answer, 'Yes')
            delete([UVL_GMML_buildPath(Mosaic.Header.Datasets(1).BasePath, Mosaic.Header.Datasets(1).CorrespPath) '/' CorrespFile]);
            fprintf('Correspondences file %s deleted !\n', [UVL_GMML_buildPath(Mosaic.Header.Datasets(1).BasePath, Mosaic.Header.Datasets(1).CorrespPath) '/' CorrespFile]);
        end

        FillUITablePairs(hObject, eventdata, handles);
    end


% --- Executes on button press in PushButtonFindCorrespondences.
function PushButtonFindCorrespondences_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonFindCorrespondences (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
