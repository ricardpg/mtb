% [General]
Param.General.SaveLog = false;                                % Save a log of the process in 'UVL_GMML_run.log'
Param.General.NumCorresp = 100;                               % Re-compute all the nodes that has less corresp than this number
Param.General.MinMatches = 25;                                % Minimum number of corresp to save the edge


% [Image preprocessing]
Param.Processing.SingleChannel = 4;                           % Transform images to single channel (0-no, 1-red, 2-green, 3-blue, 4-grayscale, 5-PCA, 6-YIQ)
Param.Processing.NonUniformLightCorrection = false;           % Correct the illumination by computing the light patern and then normalize
Param.Processing.GaussianSize = 1;                            % Radius of gaussian filter that blur the images to remove the noise (1-NoGaussianFiltering)
Param.Processing.Equalization = 1;                            % Enhence the contrast. (0-no, 1-Normalization, 2-Equalization, 3-Clahe, 4-CLAHS)
Param.Processing.ResizeCoeff = 1;                             % Resize the images to speed up the process (1-NoResize)

% [Detector]
% 1:Harris, 2:Laplacian, 3:Hessian, 4:SIFT, 5:SURF, 6:MSER, ...
% 7:Harris-Laplace, 8:Hessian-Laplace, 9:Harris-Affine, ...
% 10:Hessian-Affine, 11:Harris-Hessian-Laplace
Param.Detection.Detectors = {'Harris', 'Laplacian', 'Hessian', 'SIFT', ... % 1-4
                             'SURF', 'MSER', 'HarLap', 'HesLap', ...       % 5-8
                             'HarAff', 'HesAff', 'HarHesLap'};             % 9-11
Param.Detection.Type = [4];                                   % Type of detector used (more than one can be used)

Param.Detection.Harris.MaxNumber = 1503;                      % Maximum number of features to detect  
Param.Detection.Harris.RadiusNonMaximal = 7;                  % Minimum distance between two features (in pixels)
Param.Detection.Harris.Border = 9;                           % Border of the image in which no feature are detected (in pixels)

Param.Detection.Laplacian.MaxNumber = 1500;                   % Maximum number of features to detect
Param.Detection.Laplacian.RadiusNonMaximal = 3;               % Minimum distance between two features
Param.Detection.Laplacian.Border = 10;                        % Border of the image in which no feature are detected (in pixels)

Param.Detection.Hessian.MaxNumber = 1500;                     % Maximum number of features to detect
Param.Detection.Hessian.RadiusNonMaximal = 3;                 % Minimum distance between two features
Param.Detection.Hessian.Border = 10;                          % Border of the image in which no feature are detected (in pixels)

Param.Detection.SIFT.MaxNumber = 1500;                        % Maximum number of features to detect
Param.Detection.SIFT.RadiusNonMaximal = 3;                    % Minimum distance between two features
Param.Detection.SIFT.Border = 10;                             % Border of the image in which no feature are detected (in pixels)

Param.Detection.SURF.MaxNumber = 500;                        % Maximum number of features to detect
Param.Detection.SURF.RadiusNonMaximal = 2;                    % Minimum distance between two features
Param.Detection.SURF.Border = 10;                             % Border of the image in which no feature are detected (in pixels)

Param.Detection.MSER.MaxNumber = 1500;                        % Maximum number of features to detect
Param.Detection.MSER.RadiusNonMaximal = 3;                    % Minimum distance between two features
Param.Detection.MSER.Border = 10;                             % Border of the image in which no feature are detected (in pixels)
Param.Detection.MSER.MinimumRegionSize = 30;                  % Minimum size of a region to be detected as feature
Param.Detection.MSER.MinimumMargin = 10;                      % !!!!! NOT IMPLEMENTED YET !!!!!

Param.Detection.HarLap.MaxNumber = 1500;                      % Maximum number of features to detect
Param.Detection.HarLap.RadiusNonMaximal = 3;                  % Minimum distance between two features
Param.Detection.HarLap.Border = 10;                           % Border of the image in which no feature are detected (in pixels)

Param.Detection.HesLap.MaxNumber = 1500;                      % Maximum number of features to detect
Param.Detection.HesLap.RadiusNonMaximal = 3;                  % Minimum distance between two features
Param.Detection.HesLap.Border = 10;                           % Border of the image in which no feature are detected (in pixels)

Param.Detection.HarAff.MaxNumber = 1500;                      % Maximum number of features to detect
Param.Detection.HarAff.RadiusNonMaximal = 3;                  % Minimum distance between two features
Param.Detection.HarAff.Border = 10;                           % Border of the image in which no feature are detected (in pixels)

Param.Detection.HesAff.MaxNumber = 1500;                      % Maximum number of features to detect
Param.Detection.HesAff.RadiusNonMaximal = 3;                  % Minimum distance between two features
Param.Detection.HesAff.Border = 10;                           % Border of the image in which no feature are detected (in pixels)

Param.Detection.HarHesLap.MaxNumber = 1500;                   % Maximum number of features to detect
Param.Detection.HarHesLap.RadiusNonMaximal = 3;               % Minimum distance between two features
Param.Detection.HarHesLap.Border = 10;                        % Border of the image in which no feature are detected (in pixels)

% [Descriptors]
% 1:SIFT, 2:SURF, 3:U-SURF, 4:SURF-128, 5:U-SURF-128, ...
% 6:SURF-36, 7:U-SURF-36, 8:ImagePatch, 9:RotatedImagePatch
Param.Description.Descriptors = {'SIFT', 'SURF', 'USURF', 'SURF128', ...   % 1-4
                                 'USURF128', 'SURF36', 'USURF36', ...      % 5-7
                                 'ImagePatch', 'RotatedImagePatch'};       % 8-9
Param.Description.Type = [1];                                 % Type of descriptor used (should be the same lengh than Param.Detection.Type)
Param.Description.RelativeOrientation = 0;                    % Pprior information used to set the orientations with SURF (0-None, 1-AbsHomo, 2-Corresp) !!!!! SOULD BE USED WITH USURF !!!!!

% [Matchers]
Param.Matching.Type = 3;                                      % Type of matching used (1-NearestNeighbor, 2-SecondNN, 3-SecondNNBidirectionnal)
Param.Matching.ErrorBounds = 2;                               % Approximation for the nearest neighbor (0-none)
Param.Matching.Thresh = 1.3;                                  % With NN: distance between features. With secondNN: ratio of distances between 1st and 2nd NN

% [Outlier Rejection]
Param.Homo.Type = 1;                                          % Type of outlier rejection used (1:RansacHomo, 2-RansacFundamental, 3:LMedSHomo) !!!!! NOT IMPLEMENTED YET: USE ONLY RansacHomo !!!!!
Param.Homo.Model = 'nlp';                                     % Model of the relative Homographies ('euc' 'sim' 'aff' 'pro' 'nlp')
Param.Homo.StDev = 5;                                         % Threshold of distance between the features and the model.
Param.Homo.MaxTrials = 2000;                                  % Maximum number of sample tested in Ransac.

% [Plotting]
Param.Plotting.DoShowCorrespAfterRejection = true;           % Show Correspondences for every pairs
Param.Plotting.DoShowPoints = true;                          % Show the detected points for every images
Param.Plotting.DoShowCorrespBeforeRejection = true;          % Show Correspondences before the rejection of outliers
Param.Plotting.DoPauseAfterPlot = true;                      % Pause the process after plotting the correspondences



% [Refine] 
% Used with the function UVL_GMML_refine and UVL_GMML_refineCorrelation
Param.Refine.InitialGuess = 3;                                % Prior information used to refine correspondences (1-AbsHomo, 2-Corresp, 3-Manual)
Param.Refine.HomoModel = 'sim';                               % Model of rel Homo used as prior information with Corresp or Manual ('euc' 'sim' 'aff' 'pro' 'nlp')
% Used with the function UVL_GMML_refineCorrelation
Param.RefineCorel.CorrelRadius = 7;                           % Radius of the correlation window
Param.RefineCorel.SearchRadius = 7;                           % Radius of the search window
Param.RefineCorel.CorrelThresh = 0.8;                         % Threshold of correlation
Param.RefineCorel.PyramidLevel = 1;                           % Number of level in the multi-resolution pyramid (1-NoPyramid)
Param.RefineCorel.MaxCorresp = 500;                           % Maximum number of correspondences


% !!!!! UVL_GMML_refine and UVL_GMML_refineCorrelation should be merged in
% UVL_GMML_run using Param.Refine.InitialGuess = 0 for normal processing
