function varargout = UVLParametersSetup(varargin)
% UVLPARAMETERSSETUP MATLAB code for UVLParametersSetup.fig
%      UVLPARAMETERSSETUP, by itself, creates a new UVLPARAMETERSSETUP or raises the existing
%      singleton*.
%
%      H = UVLPARAMETERSSETUP returns the handle to a new UVLPARAMETERSSETUP or the handle to
%      the existing singleton*.
%
%      UVLPARAMETERSSETUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UVLPARAMETERSSETUP.M with the given input arguments.
%
%      UVLPARAMETERSSETUP('Property','Value',...) creates a new UVLPARAMETERSSETUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before UVLParametersSetup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to UVLParametersSetup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help UVLParametersSetup

% Last Modified by GUIDE v2.5 07-Apr-2011 09:54:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @UVLParametersSetup_OpeningFcn, ...
                   'gui_OutputFcn',  @UVLParametersSetup_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before UVLParametersSetup is made visible.
function UVLParametersSetup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to UVLParametersSetup (see VARARGIN)

% Choose default command line output for UVLParametersSetup
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes UVLParametersSetup wait for user response (see UIRESUME)
% uiwait(handles.figure1);

    global Param;

    %% load default parameters
    UVL_GMML_defaultParameters

    %% try load specific parameters for sequence
    if exist('UVL_sequenceParameters.mat', 'file')
        load('UVL_sequenceParameters.mat');
        set(handles.TextInfo, 'String', 'Using specific sequence parameters [ UVL_sequenceParameters.mat ]');
    elseif exist('UVL_sequenceParameters.m', 'file')
        UVL_sequenceParameters
        set(handles.TextInfo, 'String', 'Using specific sequence parameters [ UVL_sequenceParameters.m ]');
    else
        warning('MATLAB:UVL_GMML_run', 'No sequence specific parameters found. Using default parameters');
        set(handles.TextInfo, 'String', 'No sequence specific parameters found. Using default parameters');
    end

    UpdateParamFields(Param, handles);


% --- Outputs from this function are returned to the command line.
function varargout = UVLParametersSetup_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in CheckBoxSaveLog.
function CheckBoxSaveLog_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxSaveLog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxSaveLog



function EditNumCorresp_Callback(hObject, eventdata, handles)
% hObject    handle to EditNumCorresp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditNumCorresp as text
%        str2double(get(hObject,'String')) returns contents of EditNumCorresp as a double


% --- Executes during object creation, after setting all properties.
function EditNumCorresp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditNumCorresp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditMinMatches_Callback(hObject, eventdata, handles)
% hObject    handle to EditMinMatches (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditMinMatches as text
%        str2double(get(hObject,'String')) returns contents of EditMinMatches as a double


% --- Executes during object creation, after setting all properties.
function EditMinMatches_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditMinMatches (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditGaussianSize_Callback(hObject, eventdata, handles)
% hObject    handle to EditGaussianSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditGaussianSize as text
%        str2double(get(hObject,'String')) returns contents of EditGaussianSize as a double


% --- Executes during object creation, after setting all properties.
function EditGaussianSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditGaussianSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditEqualization_Callback(hObject, eventdata, handles)
% hObject    handle to EditEqualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditEqualization as text
%        str2double(get(hObject,'String')) returns contents of EditEqualization as a double


% --- Executes during object creation, after setting all properties.
function EditEqualization_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditEqualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditResizeCoeff_Callback(hObject, eventdata, handles)
% hObject    handle to EditResizeCoeff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditResizeCoeff as text
%        str2double(get(hObject,'String')) returns contents of EditResizeCoeff as a double


% --- Executes during object creation, after setting all properties.
function EditResizeCoeff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditResizeCoeff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuSingleChannel.
function PopUpMenuSingleChannel_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuSingleChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuSingleChannel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuSingleChannel


% --- Executes during object creation, after setting all properties.
function PopUpMenuSingleChannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuSingleChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuDetectorType.
function PopUpMenuDetectorType_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuDetectorType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuDetectorType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuDetectorType

    global Param;

    switch get(handles.PopUpMenuDetectorType, 'Value')
        case 1
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.Harris.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.Harris.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.Harris.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 2
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.Laplacian.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.Laplacian.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.Laplacian.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 3
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.Hessian.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.Hessian.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.Hessian.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 4
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.SIFT.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.SIFT.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.SIFT.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 5
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.SURF.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.SURF.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.SURF.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 6
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.MSER.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.MSER.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.MSER.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'on');
            set(handles.EditMinimumMargin, 'Visible', 'on');
            set(handles.EditMinimumRegionSize, 'String', num2str(Param.Detection.MSER.MinimumRegionSize));
            set(handles.EditMinimumMargin, 'String', num2str(Param.Detection.MSER.MinimumMargin));
        case 7
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HarLap.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HarLap.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HarLap.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 8
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HesLap.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HesLap.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HesLap.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 9
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HarAff.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HarAff.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HarAff.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 10
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HesAff.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HesAff.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HesAff.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 11
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HarHesLap.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HarHesLap.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HarHesLap.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
    end

% --- Executes during object creation, after setting all properties.
function PopUpMenuDetectorType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuDetectorType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditMaxNumber_Callback(hObject, eventdata, handles)
% hObject    handle to EditMaxNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditMaxNumber as text
%        str2double(get(hObject,'String')) returns contents of EditMaxNumber as a double


% --- Executes during object creation, after setting all properties.
function EditMaxNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditMaxNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditRadiusNonMaximal_Callback(hObject, eventdata, handles)
% hObject    handle to EditRadiusNonMaximal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditRadiusNonMaximal as text
%        str2double(get(hObject,'String')) returns contents of EditRadiusNonMaximal as a double


% --- Executes during object creation, after setting all properties.
function EditRadiusNonMaximal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditRadiusNonMaximal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditBorder_Callback(hObject, eventdata, handles)
% hObject    handle to EditBorder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditBorder as text
%        str2double(get(hObject,'String')) returns contents of EditBorder as a double


% --- Executes during object creation, after setting all properties.
function EditBorder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditBorder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuDescriptorType.
function PopUpMenuDescriptorType_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuDescriptorType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuDescriptorType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuDescriptorType


% --- Executes during object creation, after setting all properties.
function PopUpMenuDescriptorType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuDescriptorType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuRelativeOrientation.
function PopUpMenuRelativeOrientation_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuRelativeOrientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuRelativeOrientation contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuRelativeOrientation


% --- Executes during object creation, after setting all properties.
function PopUpMenuRelativeOrientation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuRelativeOrientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditStDev_Callback(hObject, eventdata, handles)
% hObject    handle to EditStDev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditStDev as text
%        str2double(get(hObject,'String')) returns contents of EditStDev as a double


% --- Executes during object creation, after setting all properties.
function EditStDev_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditStDev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditMaxTrials_Callback(hObject, eventdata, handles)
% hObject    handle to EditMaxTrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditMaxTrials as text
%        str2double(get(hObject,'String')) returns contents of EditMaxTrials as a double


% --- Executes during object creation, after setting all properties.
function EditMaxTrials_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditMaxTrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuHomoType.
function PopUpMenuHomoType_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuHomoType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuHomoType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuHomoType


% --- Executes during object creation, after setting all properties.
function PopUpMenuHomoType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuHomoType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuHomoModel.
function PopUpMenuHomoModel_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuHomoModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuHomoModel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuHomoModel


% --- Executes during object creation, after setting all properties.
function PopUpMenuHomoModel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuHomoModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditErrorBounds_Callback(hObject, eventdata, handles)
% hObject    handle to EditErrorBounds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditErrorBounds as text
%        str2double(get(hObject,'String')) returns contents of EditErrorBounds as a double


% --- Executes during object creation, after setting all properties.
function EditErrorBounds_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditErrorBounds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditThresh_Callback(hObject, eventdata, handles)
% hObject    handle to EditThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditThresh as text
%        str2double(get(hObject,'String')) returns contents of EditThresh as a double


% --- Executes during object creation, after setting all properties.
function EditThresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuMatchingType.
function PopUpMenuMatchingType_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuMatchingType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuMatchingType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuMatchingType


% --- Executes during object creation, after setting all properties.
function PopUpMenuMatchingType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuMatchingType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CheckBoxDoShowCorrespAfterRejection.
function CheckBoxDoShowCorrespAfterRejection_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxDoShowCorrespAfterRejection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxDoShowCorrespAfterRejection



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CheckBoxDoShowPoints.
function CheckBoxDoShowPoints_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxDoShowPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxDoShowPoints


% --- Executes on button press in CheckBoxDoShowCorrespBeforeRejection.
function CheckBoxDoShowCorrespBeforeRejection_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxDoShowCorrespBeforeRejection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxDoShowCorrespBeforeRejection


% --- Executes on button press in CheckBoxDoPauseAfterPlot.
function CheckBoxDoPauseAfterPlot_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxDoPauseAfterPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxDoPauseAfterPlot


% --- Executes on selection change in PopUpMenuRefineHomoModel.
function PopUpMenuRefineHomoModel_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuRefineHomoModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuRefineHomoModel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuRefineHomoModel


% --- Executes during object creation, after setting all properties.
function PopUpMenuRefineHomoModel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuRefineHomoModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuInitialGuess.
function PopUpMenuInitialGuess_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuInitialGuess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuInitialGuess contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuInitialGuess


% --- Executes during object creation, after setting all properties.
function PopUpMenuInitialGuess_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuInitialGuess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditCorrelRadius_Callback(hObject, eventdata, handles)
% hObject    handle to EditCorrelRadius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditCorrelRadius as text
%        str2double(get(hObject,'String')) returns contents of EditCorrelRadius as a double


% --- Executes during object creation, after setting all properties.
function EditCorrelRadius_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditCorrelRadius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditSearchRadius_Callback(hObject, eventdata, handles)
% hObject    handle to EditSearchRadius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditSearchRadius as text
%        str2double(get(hObject,'String')) returns contents of EditSearchRadius as a double


% --- Executes during object creation, after setting all properties.
function EditSearchRadius_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditSearchRadius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditCorrelThresh_Callback(hObject, eventdata, handles)
% hObject    handle to EditCorrelThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditCorrelThresh as text
%        str2double(get(hObject,'String')) returns contents of EditCorrelThresh as a double


% --- Executes during object creation, after setting all properties.
function EditCorrelThresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditCorrelThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditPyramidLevel_Callback(hObject, eventdata, handles)
% hObject    handle to EditPyramidLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditPyramidLevel as text
%        str2double(get(hObject,'String')) returns contents of EditPyramidLevel as a double


% --- Executes during object creation, after setting all properties.
function EditPyramidLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditPyramidLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditMaxCorresp_Callback(hObject, eventdata, handles)
% hObject    handle to EditMaxCorresp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditMaxCorresp as text
%        str2double(get(hObject,'String')) returns contents of EditMaxCorresp as a double


% --- Executes during object creation, after setting all properties.
function EditMaxCorresp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditMaxCorresp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CheckBoxNonUniformLightCorrection.
function CheckBoxNonUniformLightCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxNonUniformLightCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxNonUniformLightCorrection



function EditMinimumRegionSize_Callback(hObject, eventdata, handles)
% hObject    handle to EditMinimumRegionSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditMinimumRegionSize as text
%        str2double(get(hObject,'String')) returns contents of EditMinimumRegionSize as a double


% --- Executes during object creation, after setting all properties.
function EditMinimumRegionSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditMinimumRegionSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditMinimumMargin_Callback(hObject, eventdata, handles)
% hObject    handle to EditMinimumMargin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditMinimumMargin as text
%        str2double(get(hObject,'String')) returns contents of EditMinimumMargin as a double


% --- Executes during object creation, after setting all properties.
function EditMinimumMargin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditMinimumMargin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuEqualization.
function PopUpMenuEqualization_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuEqualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuEqualization contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuEqualization


% --- Executes during object creation, after setting all properties.
function PopUpMenuEqualization_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuEqualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PushButtonSaveParameters.
function PushButtonSaveParameters_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonSaveParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Param;

    Param = StoreParamFields(Param, handles);
    save('UVL_SequenceParameters.mat', 'Param');


% --- Executes on button press in PushButtonLoadParameters.
function PushButtonLoadParameters_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonLoadParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global Param;

    if exist('UVL_sequenceParameters.mat', 'file')
        load('UVL_sequenceParameters.mat');
        set(handles.TextInfo, 'String', 'Using specific sequence parameters [ UVL_sequenceParameters.mat ]');
    elseif exist('UVL_sequenceParameters.m', 'file')
        UVL_sequenceParameters
        set(handles.TextInfo, 'String', 'Using specific sequence parameters [ UVL_sequenceParameters.m ]');
    else
        warning('MATLAB:UVL_GMML_run', 'No sequence specific parameters found. Using default parameters');
        set(handles.TextInfo, 'String', 'No sequence specific parameters found. Using default parameters');
    end

    UpdateParamFields(Param, handles);
