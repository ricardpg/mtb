function UpdateParamFields(Param, handles)

    % [General]
    set(handles.CheckBoxSaveLog, 'Value', Param.General.SaveLog);
    set(handles.EditNumCorresp, 'String', num2str(Param.General.NumCorresp));
    set(handles.EditMinMatches, 'String', num2str(Param.General.MinMatches));

    % [Image preprocessing]
    set(handles.PopUpMenuSingleChannel, 'Value', Param.Processing.SingleChannel + 1);
    set(handles.CheckBoxNonUniformLightCorrection, 'Value', Param.Processing.NonUniformLightCorrection);
    set(handles.EditGaussianSize, 'String', num2str(Param.Processing.GaussianSize));
    set(handles.PopUpMenuEqualization, 'Value', Param.Processing.Equalization + 1);
    set(handles.EditResizeCoeff, 'String', num2str(Param.Processing.ResizeCoeff));

    % [Detector]
    set(handles.PopUpMenuDetectorType, 'Value', Param.Detection.Type);
    
    switch Param.Detection.Type
        case 1
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.Harris.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.Harris.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.Harris.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 2
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.Laplacian.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.Laplacian.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.Laplacian.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 3
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.Hessian.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.Hessian.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.Hessian.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 4
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.SIFT.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.SIFT.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.SIFT.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 5
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.SURF.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.SURF.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.SURF.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 6
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.MSER.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.MSER.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.MSER.Border));
            set(handles.EditMinimumRegionSize, 'String', num2str(Param.Detection.MSER.MinimumRegionSize));
            set(handles.EditMinimumMargin, 'String', num2str(Param.Detection.MSER.MinimumMargin));
            set(handles.EditMinimumRegionSize, 'Visible', 'on');
            set(handles.EditMinimumMargin, 'Visible', 'on');
            set(handles.EditMinimumRegionSize, 'String', num2str(Param.Detection.MSER.MinimumRegionSize));
            set(handles.EditMinimumMargin, 'String', num2str(Param.Detection.MSER.MinimumMargin));
        case 7
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HarLap.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HarLap.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HarLap.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 8
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HesLap.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HesLap.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HesLap.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 9
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HarAff.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HarAff.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HarAff.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 10
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HesAff.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HesAff.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HesAff.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
        case 11
            set(handles.EditMaxNumber, 'String', num2str(Param.Detection.HarHesLap.MaxNumber));
            set(handles.EditRadiusNonMaximal, 'String', num2str(Param.Detection.HarHesLap.RadiusNonMaximal));
            set(handles.EditBorder, 'String', num2str(Param.Detection.HarHesLap.Border));
            set(handles.EditMinimumRegionSize, 'Visible', 'off');
            set(handles.EditMinimumMargin, 'Visible', 'off');
    end

    % [Descriptors]
    set(handles.PopUpMenuDescriptorType, 'Value', Param.Description.Type);
    set(handles.PopUpMenuRelativeOrientation, 'Value', Param.Description.RelativeOrientation + 1);

    % [Matchers]
    set(handles.PopUpMenuMatchingType, 'Value', Param.Matching.Type);
    set(handles.EditErrorBounds, 'String', num2str(Param.Matching.ErrorBounds));
    set(handles.EditThresh, 'String', num2str(Param.Matching.Thresh));

    % [Outlier Rejection]
    set(handles.PopUpMenuHomoType, 'Value', Param.Homo.Type);

    switch Param.Homo.Model
        case 'euc'
            set(handles.PopUpMenuHomoModel, 'Value', 1);
        case 'sim'
            set(handles.PopUpMenuHomoModel, 'Value', 2);
        case 'aff'
            set(handles.PopUpMenuHomoModel, 'Value', 3);
        case 'pro'
            set(handles.PopUpMenuHomoModel, 'Value', 4);
        case 'nlp'
            set(handles.PopUpMenuHomoModel, 'Value', 5);            
    end

    set(handles.EditStDev, 'String', num2str(Param.Homo.StDev));
    set(handles.EditMaxTrials, 'String', num2str(Param.Homo.MaxTrials));

    % [Plotting]
    set(handles.CheckBoxDoShowCorrespAfterRejection, 'Value', Param.Plotting.DoShowCorrespAfterRejection);
    set(handles.CheckBoxDoShowPoints, 'Value', Param.Plotting.DoShowPoints);
    set(handles.CheckBoxDoShowCorrespBeforeRejection, 'Value', Param.Plotting.DoShowCorrespBeforeRejection);
    set(handles.CheckBoxDoPauseAfterPlot, 'Value', Param.Plotting.DoPauseAfterPlot);

    % [Refine]
    set(handles.PopUpMenuInitialGuess, 'Value', Param.Refine.InitialGuess);
    
    switch Param.Refine.HomoModel
        case 'euc'
            set(handles.PopUpMenuRefineHomoModel, 'Value', 1);
        case 'sim'
            set(handles.PopUpMenuRefineHomoModel, 'Value', 2);
        case 'aff'
            set(handles.PopUpMenuRefineHomoModel, 'Value', 3);
        case 'pro'
            set(handles.PopUpMenuRefineHomoModel, 'Value', 4);
        case 'nlp'
            set(handles.PopUpMenuRefineHomoModel, 'Value', 5);            
    end

    set(handles.EditCorrelRadius, 'String', num2str(Param.RefineCorel.CorrelRadius));
    set(handles.EditSearchRadius, 'String', num2str(Param.RefineCorel.SearchRadius));
    set(handles.EditCorrelThresh, 'String', num2str(Param.RefineCorel.CorrelThresh));
    set(handles.EditPyramidLevel, 'String', num2str(Param.RefineCorel.PyramidLevel));
    set(handles.EditMaxCorresp, 'String', num2str(Param.RefineCorel.MaxCorresp));

    % !!!!! UVL_GMML_refine and UVL_GMML_refineCorrelation should be merged in
    % UVL_GMML_run using Param.Refine.InitialGuess = 0 for normal processing

end